* v12 -
   - transition release, to make redirection for Seafile 12 links

* v11 -
   - archive release of the old v0.1 release (for Seafile 11)

* v0.1 - 
    - initial release
