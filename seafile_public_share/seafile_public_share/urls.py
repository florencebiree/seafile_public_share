# -*- coding: utf-8 -*-
###############################################################################
#       seafformsite/urls.py
#       
#       Copyright © 2023-2025, Florence Birée <florence@biree.name>
#       
#       This file is a part of seafile_public_share.
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as 
#       published by the Free Software Foundation, either version 3 of the 
#       License, or (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#       
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""seafile_public_share URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

__author__ = "Florence Birée"
__version__ = "12"
__license__ = "AGPLv3"
__copyright__ = "Copyright © 2023-2025, Florence Birée <florence@biree.name>"

from django.contrib import admin
from django.urls import include, path
from django.conf import settings

urlpatterns = [
    path('{}admin/'.format(settings.SPS_ROOT), admin.site.urls),
    path('{}'.format(settings.SPS_ROOT), include('seafile_public_wopi_server.urls')), 
]
