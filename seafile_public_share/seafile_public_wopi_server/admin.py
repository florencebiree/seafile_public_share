# -*- coding: utf-8 -*-
###############################################################################
#       seafile_public_share/admin.py
#       
#       Copyright © 2023-2025, Florence Birée <florence@biree.name>
#       
#       This file is a part of seafile_public_share.
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as 
#       published by the Free Software Foundation, either version 3 of the 
#       License, or (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#       
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Seafile_public_share admin site description"""

__author__ = "Florence Birée"
__version__ = "12"
__license__ = "AGPLv3"
__copyright__ = "Copyright © 2023-2025, Florence Birée <florence@biree.name>"

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.admin import AdminSite
from django.contrib.auth.models import User
from django.utils.translation import gettext as _

# Register your models here.
from . import models

# Customize the form administration
class SeafilePubShareLinkAdmin(admin.ModelAdmin):
    list_display = ('creation_datetime', 'owner', 'filepath')
    list_filter = ('owner',)
    readonly_fields = ('creation_datetime',)
    search_fields = ['filepath']
    view_on_site = True

# Define an inline admin descriptor for SeafileUserInline model
# which acts a bit like a singleton
class SeafileUserInline(admin.StackedInline):
    model = models.SeafileUser
    can_delete = False
    verbose_name_plural = 'Seafile users'

# Define a new User admin
class UserAdmin(UserAdmin):
    inlines = (SeafileUserInline, )

# Customize the admin site
class SPSAdminSite(AdminSite):
    site_header = _('Seafile public share administration')

#admin_site = SPSAdminSite('SPS admin')

# Register Form
#admin_site.register(models.SeafilePubShareLink, SeafilePubShareLinkAdmin)
admin.site.register(models.SeafilePubShareLink, SeafilePubShareLinkAdmin)

admin.site.register(models.SeafileUser)

# Re-register UserAdmin
#admin_site.register(User, UserAdmin)
#admin.site.register(User, UserAdmin)

