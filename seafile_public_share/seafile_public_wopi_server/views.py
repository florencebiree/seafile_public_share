# -*- coding: utf-8 -*-
###############################################################################
#       seafile_public_wopi_server/views.py
#       
#       Copyright © 2023-2025, Florence Birée <florence@biree.name>
#       
#       This file is a part of seafile_public_share.
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as 
#       published by the Free Software Foundation, either version 3 of the 
#       License, or (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#       
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Seafile public share, wopi server views

In v12, there is no more wopi server, just redirection to the Seafile one
"""

__author__ = "Florence Birée"
__version__ = "12"
__license__ = "AGPLv3"
__copyright__ = "Copyright © 2023-2025, Florence Birée <florence@biree.name>"

import os
from django.db import IntegrityError
from urllib.parse import urljoin, quote_plus
from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from seafile_public_wopi_server.models import SeafileUser, SeafilePubShareLink
from seafile_public_wopi_server.seafile_webapi import SeafileClient, AuthError, APIError, InvalidToken, AuthError, BadPath, NotFound
from django.utils.translation import gettext as _
from django.conf import settings
from django.http import HttpResponseRedirect, Http404
from django.urls import reverse
#from .wopi.utils import get_wopi_dict
import logging

logger = logging.getLogger(__name__)

def _log_by_seafile_session(request):
    """ Authenticate or create a new account, by using Seafile session
    
    Return if the login is successful
    """
    seaf_root = settings.SEAFILE_ROOT
    
    # only try if a 'sessionid' cookie is set
    if not 'sessionid' in request.COOKIES:
        logger.info('Auth: No sessionid in cookies')
        return False
    
    # try to connect to seafile using cookies
    seaf = SeafileClient(seaf_root, v12auth=True)
    try:
        seaf.authenticate_cookies(request.COOKIES) # may raise AuthError
    except AuthError as e:
        logger.error('Auth: seafile auth cookies error, {}'.format(e))
        return False
    token = seaf.token
    if not token:
        logger.error('Auth: no token given by seafile api')
        return False
    # get user profile
    seafile_profile = seaf.user_my_profile()
    email = seafile_profile['email']
    logger.info('Auth: logged as {}'.format(email))
        
    # create new user, save the token / or update existing user
    try:
        user = User.objects.create_user(email, email, token)
    except IntegrityError:
        logger.info('Auth: already existing user, updating…')
        # the user already exist, we want to update it
        user = User.objects.get(username = email)
        user.set_password(token) # we use the token as password… re-defined each time
        user.save()
        seafu = user.seafileuser
        seafu.seaftoken = token
        seafu.update(seafile_profile, save=False)
        seafu.save()
    else:
        # new user
        logger.info('Auth: new user')
        user.save()
        seafuser = SeafileUser(user=user, seafroot=seaf_root,
                               seaftoken=token)
        seafuser.update(seafile_profile, save=False)
        seafuser.save()
    # login
    user = authenticate(username=email, password=token)
    login(request, user)
    logger.info('Auth: success for {}'.format(email))
    return True

def _need_auth(func):
    """Decorator to check auth status"""
    def _authenticate_or_login(request):
        """Check if we are authenticated, else do it through cookies or redirect
            to the Seafile authentication page
        """
        # Check if we are already authenticated
        #if not request.user.is_authenticated:
            # if not, try to authenticate using existing cookies
        
        # always try a cookie auth, don't trust a token…
        cookies_auth = _log_by_seafile_session(request)
        logger.info('cookie auth result: {}'.format(cookies_auth))
        # if that failed, redirect to the seafile login page 
        if not cookies_auth:
            logger.info('redirect to seafile login page')
            return HttpResponseRedirect(
                urljoin(settings.SEAFILE_ROOT, 'accounts/login/') +
                '?next=' + request.path
            )
        # other cases: run func(request)
        return func(request)
    
    return _authenticate_or_login 

@_need_auth
def index(request):
    """Main private view, showing user existing shares"""

    # Get SeafileUser
    seafu = request.user.seafileuser
    seaf = SeafileClient(settings.SEAFILE_ROOT, v12auth=True)
    seaf.authenticate(seafu.user.username, token=seafu.seaftoken)
        
    # we are authenticated !
    # get all links
    sps_link_list = SeafilePubShareLink.objects.filter(owner=request.user).order_by('-creation_datetime')
    
    return render(request, 'sps_wopi_server/index.html', {
        'seaf_root': settings.SEAFILE_ROOT,
        'seaf_media': settings.SEAFILE_MEDIA,
        'user': request.user,
        'sps_link_list': sps_link_list,
        'deleted': request.GET.get('deleted', None),
    })

@_need_auth
def logout_view(request):
    """Log out in Seafile public share, then in Seafile"""
    logout(request)
    # if we use SEAFILE_SESSION_LOGIN, also disconnect in Seafile
    return HttpResponseRedirect(
        urljoin(settings.SEAFILE_ROOT, 'accounts/logout/') 
    )

@_need_auth
def edit_link(request):
    """View to create/edit a link (if a link doesn't exists, it will be created"""

    # Get SeafileUser
    seafu = request.user.seafileuser
    
    if request.method == 'POST': #delete the edit_link
        if 'deleteid' in request.POST:
            share_id_to_delete = request.POST.get('deleteid', '')
            try:
                tobedeleted = SeafilePubShareLink.objects.get(shareid=share_id_to_delete, owner=request.user)
            except SeafilePubShareLink.DoesNotExist:
                # do nothing
                pass
            else:
                # delete and redirect
                delete_name = tobedeleted.pretty_path()
                tobedeleted.delete()
                return HttpResponseRedirect(
                    reverse('index') + '?deleted=' + quote_plus(delete_name)
                )
    
    # try to get the link
    try:
        repo_id = request.GET['repo_id']
        file_path = request.GET['file_path']
    except KeyError:
        raise Http404("Bad link data.")
    back_url = request.GET.get('back', reverse('index'))
    
    try:
        sps_link = SeafilePubShareLink.objects.get(repoid=repo_id, filepath=file_path, owner=request.user)
        status = _("Editing an existing link.")
        
        # if no existing link, try to create a new one.
    except SeafilePubShareLink.DoesNotExist:
        # check if we have an office file
        ext = os.path.splitext(file_path)[1][1:] # we remove the '.'
        if not ext in settings.OFFICE_WEB_APP_EDIT_FILE_EXTENSION:
            return render(request, 'sps_wopi_server/edit_link_bad_type.html', {
                'seaf_root': settings.SEAFILE_ROOT,
                'seaf_media': settings.SEAFILE_MEDIA,
                'user': request.user,
                'back_url': back_url,
                'file_path': file_path,
            })
        
        seaf = SeafileClient(settings.SEAFILE_ROOT, v12auth=True)
        seaf.authenticate(seafu.user.username, token=seafu.seaftoken)
        # check if the file exists
        try:
            seaf.stat_file(repo_id, file_path)
        except:
            #raise Http404("Bad file.")
            raise
        # get the repo name
        repo_info = seaf.repo_info(repo_id) 
        
        # create a new share link
        sps_link = SeafilePubShareLink()
        sps_link.owner = request.user
        sps_link.filepath = file_path
        sps_link.repoid = repo_id
        sps_link.reponame = repo_info['name']
        sps_link.save()
        
        status = _("New editing link created.") 
    
    return render(request, 'sps_wopi_server/edit_link.html', {
        'seaf_root': settings.SEAFILE_ROOT,
        'seaf_media': settings.SEAFILE_MEDIA,
        'user': request.user,
        'sps_link': sps_link,
        'status': status,
        'back_url': back_url,
        'basename': os.path.basename(sps_link.filepath),
    })
    

def shareview(request, shareid):
    """
    
    Old behavior: display a wopi editor for `shareid`
    
    Now (Seafile 12+):
        - if there is already a v12 link, redirect to it
            (if the link is disabled in Seafile 12, continue to redirect on it,
             this allow user to remove links in the normal Seafile 12 interface)
        - if no v12 link, get one, then redirect to it.
    
    """
    # Get the share link object
    try:
        sps_link = SeafilePubShareLink.objects.get(shareid=shareid)
    except:
        raise Http404("Bad link data.")
        
    # If no v12 link, try to create one
    if not sps_link.v12link:
        # Get SeafileUser
        seafu = sps_link.owner.seafileuser
        seaf = SeafileClient(settings.SEAFILE_ROOT, v12auth=True)
        seaf.authenticate(seafu.user.username, token=seafu.seaftoken)
        
        try:
            response = seaf.create_multi_share_link(sps_link.repoid, sps_link.filepath, can_edit=True)
        except BadPath as err:
            raise Http404("Cant create v12 edit link: %s" % err.msg)
        except NotFound as err:
            # the file no more exist on the server, delete the link and redirect
            sps_link.delete()
            #return HttpResponseRedirect(settings.SEAFILE_ROOT)
            raise Http404("This file doesn't exist anymore.")
            
        sps_link.v12link = response['link']
        sps_link.save()
    
    # Redirect to the v12 edit link
    return HttpResponseRedirect(sps_link.v12link)


def handler404(request, exception):
    response = render(request, '404.html', {
        'seaf_root': settings.SEAFILE_ROOT,
        'seaf_media': settings.SEAFILE_MEDIA,
    })
    response.status_code = 404
    return response

def info(request):
    """Return an info page for old edit links, with a redirect to a download link"""
    try:
        share_link = request.GET['url']
    except KeyError:
        raise Http404("Bad share link.")
    
    return render(request, 'sps_wopi_server/info.html', {
        'seaf_root': settings.SEAFILE_ROOT,
        'seaf_media': settings.SEAFILE_MEDIA,
        'share_link': share_link,
    })

