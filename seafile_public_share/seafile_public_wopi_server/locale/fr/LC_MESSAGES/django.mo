��    )      d  ;   �      �  X   �  U   �  R  H  �   �     #     2     7     G     N     V     h     q     �     �     �     �     �     �  	   �  #   �  j     #   �     �     �     �  7   �  	   /     9     P  	   g     q     z          �     �     �     �     �     �     �  -  	  G   5
  X   }
    �
  �   �     �     �     �  	   �     �     �  	   �     �  "        (  	   0     :     T     q     z  '   �  q   �  +   "  &   N  )   u     �  6   �  	   �     �          (     :     S     W  !   e     �     �     �  ,   �     �  %                 #      &                	                
              !      $          "                                              '      )                                         %             (           
                The file %(file_path)s is not an online editable document.
             
            The name or surname others will see when you edit the document:
         
            This tool use
            <a href="https://en.wikipedia.org/wiki/Free_software">free
            softwares</a>: 
            <a href="http://seafile.com">Seafile</a> and 
            <a href="https://framagit.org/florencebiree/seafile_public_share">Seafile
            public share (give me the source code)</a>!
             
        This page doesn't exists. Maybe it's a link to a moved or deleted file?
        Maybe the link was deleted by it's owner.
     Administration Back Back to Seafile Delete Details Edit the document Editable Editing an existing link. Existing public edit links File File: New Seafile 12 link: New editing link created. Nothing here Old link: Open the document as public edition Public edit link for <code class="w3-codespan w3-text-black" style="font-size: 0.8em;">%(basename)s</code> Seafile public share administration Seafile: your public edit links Seafile: your public edit share Sharing date The edition link to %(deleted)s was removed succesfully Your name Your public edit links creation date and time file path login id name owner seafile library id seafile library name seafile server seafile token seafile v12 cloud edit link share link id user contact email Project-Id-Version: 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Florence Birée <florence@biree.name>
Language-Team: 
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 
Le fichier %(file_path)s n'est pas un document éditable en ligne.
    
Le nom ou le pseudo que les autres vont voir pendant que vous éditez le document :
    
Cet outil utilise des <a href="https://en.wikipedia.org/wiki/Free_software">logiciels libres</a> :
<a href="http://seafile.com">Seafile</a> et 
<a href="https://framagit.org/florencebiree/seafile_public_share">Seafile
public share (donnez moi le code source)</a>.
    
       Cette page n'existe pas. Peut-être que c'est un lien vers un fichier déplacé ou supprimé ?
       Peut-être que le lien a été supprimé par son propriétaire ?
    Administration Retour Retour à Seafile Supprimer Détails Éditer le document Éditable Lien déjà existant. Liens publics d'édition existants Fichier Fichier : Nouveau lien Seafile 12 : Création d'un nouveau lien. Rien ici Ancien lien : Ouvrir le document en édition publique Lien public d'édition pour <code class="w3-codespan w3-text-black" style="font-size: 0.8em;">%(basename)s</code> Administration des partages publics Seafile Seafile : vos liens publics d'édition Seafile : vos partages publics d'édition Date de partage Le lien d'édition de %(deleted)s a été désactivé. Votre nom Vos liens publics d'édition date et heure de création chemin du fichier identifiant de connexion nom propriétaire identifiant bibliothèque seafile nom de bibliothèque seafile serveur seafile token seafile Lien d'édition dans le cloud de Seafile v12 identifiant du lien de partage E-mail de contact de l'utilisateurice 