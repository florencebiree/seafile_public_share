# -*- coding: utf-8 -*-
###############################################################################
#       seafile_public_share/seafile_public_wopi_server/models.py
#       
#       Copyright © 2023-2025, Florence Birée <florence@biree.name>
#       
#       This file is a part of seafile_public_share.
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as 
#       published by the Free Software Foundation, either version 3 of the 
#       License, or (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#       
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Seafile public share models"""

__author__ = "Florence Birée"
__version__ = "12"
__license__ = "AGPLv3"
__copyright__ = "Copyright © 2023-2025, Florence Birée <florence@biree.name>"

import os
import uuid
from urllib.parse import urljoin
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext as _
from django.urls import reverse
from django.conf import settings

class SeafileUser(models.Model):
    """Profile class for user
    
    We handle the three possible login in seafile:
        using `email` (stored in user.username), root seafile ID
        using `login_id` (optional)
        using `contact_email` (optional)
    
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    seafroot = models.URLField(_("seafile server"))
    seaftoken = models.CharField(_("seafile token"), max_length=40)
    
    login_id = models.CharField(_("login id"), blank=True, default="", max_length=254)
    contact_email = models.EmailField(_("user contact email"), blank=True, default="")
    
    name = models.CharField(_("name"), max_length=254, blank=True, default="")
    
    def __str__(self):
        return "<SeafileProfile(%s)>" % self.user.email
    
    def get_email(self):
        """Return the email used as seafile root id"""
        return self.user.username
    
    def get_name_or_email(self):
        """Return the name, or the email if no name given"""
        return self.name if self.name else self.get_contact_email()
    
    def get_contact_email(self):
        """Return the prefered contact email"""
        return self.contact_email if self.contact_email else self.user.username
    
    def update(self, seafile_profile, save=True):
        """Update SeafileUser with profile data from Seafile"""
        _un_none_ify = (lambda x: '' if x is None else x)
        self.login_id = _un_none_ify(seafile_profile.get('login_id', ''))
        self.name = _un_none_ify(seafile_profile.get('name', ''))
        self.contact_email = _un_none_ify(seafile_profile.get('contact_email', ''))
        if save:
            self.save()

class SeafilePubShareLink(models.Model):
    """Class for a public share link"""
    owner = models.ForeignKey(User, verbose_name=_("owner"), on_delete=models.CASCADE)
    filepath = models.CharField(_("file path"), max_length=256)
    repoid = models.CharField(_("seafile library id"), max_length=40)
    reponame = models.CharField(_("seafile library name"), max_length=256)
    creation_datetime = models.DateTimeField(_("creation date and time"), auto_now_add=True)
    shareid = models.UUIDField(_("share link id"), default=uuid.uuid4, primary_key=True, editable=False)
    editable = models.BooleanField(_("Editable"), default=True)
    v12link = models.CharField(_("seafile v12 cloud edit link"), max_length=256, blank=True)
    
    def url(self):
        """Return the url of the share link"""
        return urljoin(settings.SEAFILE_ROOT, reverse('shareview', args=[self.shareid]))
    
    def pretty_path(self):
        """Return a path to be displayed withe reponame and filepath"""
        return os.path.join(self.reponame, self.filepath[1:])
