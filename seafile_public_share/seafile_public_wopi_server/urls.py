# -*- coding: utf-8 -*-
###############################################################################
#       seafile_public_wopi_server/urls.py
#       
#       Copyright © 2023-2025, Florence Birée <florence@biree.name>
#       
#       This file is a part of Seafile public share.
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as 
#       published by the Free Software Foundation, either version 3 of the 
#       License, or (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#       
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Seafile public wopi server URL dispatcher"""

__author__ = "Florence Birée"
__version__ = "12"
__license__ = "AGPLv3"
__copyright__ = "Copyright © 2023-2025, Florence Birée <florence@biree.name>"

from django.urls import re_path, path, include

from seafile_public_wopi_server import views

urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'edit_link/$', views.edit_link, name='edit_link'),
    re_path(r'^logout/$', views.logout_view, name='logout'),
    re_path(r'^s/(?P<shareid>[^/]*)/$', views.shareview, name='shareview'),
    re_path(r'info/$', views.info, name="info"),
]

