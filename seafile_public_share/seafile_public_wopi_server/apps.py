from django.apps import AppConfig


class SeafilePublicWopiServerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'seafile_public_wopi_server'
