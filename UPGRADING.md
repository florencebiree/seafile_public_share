# Seafile public share - upgrade instructions

## Upgrading from an older Seafile public share version

Check first the dependency, especially the django version installed on your
system.

If you get the code using git, you can update it using:

    $ git pull

You must take a look on seafile_public_share/seafile_public_share/settings.py 
It may be simpler to make a backup of your old settings.py, copy the new
settings.py.sample to settings.py, and retreive your settings from the old one.

Then, you must update your database scheme :

    $ cd seafile_public_share/
    $ . bin/activate                                                                 
    $ ./manage.py migrate

and the static files :

    $ ./manage.py collectstatic

## Upgrading your virtual environment after a system update

When you upgrade your system, you may change your Python version. This can
break your virtual environment, and thus SeafForm.

You need to re-create your virtual environment:

    $ virtualenv --python=python3 --system-site-packages seafile_public_share/                   
    $ cd seafile_public_share/ # go to the virtual env to install python packages                
    $ . bin/activate                                                             
    $ pip install gunicorn # and maybe other dependencies


                 
