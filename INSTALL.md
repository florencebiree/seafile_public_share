# Seafile public share - installation instructions

## Requierments

- Python 3
- Python virtual environments
- Django 3.2
- Python3-requests
- python3-djangorestframework

You need to install Seafile public share in the same server and host than
Seafile, because Seafile public share use Seafile cookies for authentication.
Also make sure you use HTTPS ;)

## How to install it?

### Get the code
*put it in a directory not in your server document root, lets says:*

    $ cd /opt/
    $ git clone https://framagit.org/florencebiree/seafile_public_share.git
    $ cd seafile_public_share

### Dependencies
*here are the Debian/Bookworm packages, if needed libs are not packaged in
you distribution, install them using pip in the virtualenv*

    # apt install python3 python3-virtualenv python3-django python3-requests virtualenv python3-djangorestframework npm

### Initialize the virtual environment and dependencies

    $ # in the seafile_public_share directory
    $ virtualenv --python=python3 --system-site-packages seafile_public_share/
    $ cd seafile_public_share/
    $ . bin/activate
    $ # if you need some other dependencies:
    $ pip install daphne
    $ # if you have an attrs import error:
    $ pip install --upgrade attrs

## Configuring the app

In seafile_public_share/seafile_public_share/seafile_public_share copy
settings.py.sample to settings.py, and change it for your needs (read the
whole file to find settings you need to change).

*You must read the Django checklist to configure SeafForm for production!*
https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

For static files, you must create a static directory, configure STATIC_ROOT
and STATIC_URL to point to this directory system path and URL. 

You must initialize you database and the static files using:
    
    $ ./manage.py migrate
    $ ./manage.py collectstatic

### Starting the app
*You can use Daphne to run SeafForm, and start it with a process manager like supervisord*

See doc/seafile_public_share-supervisor.conf for a supervisord configuration snippet.

### Configure your webserver

Tell your webserver (Apache, Nginx) to communicate to the seafile_public_share
socket. And be sure you use only HTTPS (with HSTS if possible).

See doc/seafile_public_share-nginx for an nginx configuration snippet.

### Configure Seafile

To allow the authentication in Seafile using session cookies, you need to
enable in your seahub_settings.py:
ENABLE_GET_AUTH_TOKEN_BY_SESSION = True

You can also add a links at the bottom with ADDITIONAL_APP_BOTTOM_LINKS or in
the left bar with CUSTOM_NAV_ITEMS

## Test!

It should works!

## Use the administration interface

To control all the links created by Seafile public share, you can use the
adminstration interface.

You need to create a superuser account:

    $ cd seafile_public_share/
    $ . bin/activate
    $ ./manage.py createsuperuser

This superuser account allow you to connect to /admin.

In the administration interface, you can also give admin right to a normal
account. Go in the User list, find this user, and give him "staff" and 
"superuser" rights.

Then, if you are connected in Seafile public share using this user, you will
find an "Administration" link on the top navigation bar.

