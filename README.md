# seafile_public_share

*This tool is not needed anymore with Seafile 12!*

Seafile 12 Community Edition download links are directly able to propose cloud
edition mode.

If you want to use it with Seafile <= 11, you can use the (unmaintened)
branch v11:
https://framagit.org/florencebiree/seafile_public_share/-/tree/v11

## Initial usage

This is a tool to add a feature to the Seafile community edition, the ability
to create share links with anonymous edit permissions, in a wopi office
software (like Collabora Online).

## Current main version

The current v12 version, is a transition version for Seafile >=12.
It allow old seafile_public_share links to be redirected new Seafile 12
download links with cloud edition enabled.

## How to use it

You must have access to a Seafile instance 12. Then follow INSTALL.md
instructions  to install seafile_public_share in a subdirectory of your server. 
You will need Python 3 and Django:
https://framagit.org/florencebiree/seafile_public_share/blob/master/INSTALL.md

Seafile public share is released under the terms of the Affero General Public
License (v3+), so you must show a link to the source code to all your users.
Read the COPIYING file for more informations.

## How to upgrade

Read upgrading instructions here:
https://framagit.org/florencebiree/seafile_public_share/-/blob/master/UPGRADING.md
